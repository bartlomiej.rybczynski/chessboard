import javax.swing.*;

public class GameRow {
    public int gamerowid;



    public int getGamerowid() {
        return gamerowid;
    }

    public void setGamerowid(int gamerowid) {
        this.gamerowid = gamerowid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String value;
    public String name;
//
//    public GameRow(int value,String name, int gamerowid){
//        setValue(value);
//        setName(name);
//        setGamerowid(gamerowid);
//
//    }

    public GameRow(String value,String name, int gamerowid){
        setValue(value);
        setName(name);
        setGamerowid(gamerowid);

    }

}
