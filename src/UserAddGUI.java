import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class UserAddGUI  extends JPanel {

//    DB db ;
    private JTextField userNameField;
    private JButton dodajButton;
    private JButton cancelButton;
    private JLabel usernameLabel;

    public UserAddGUI() {
        DB db = new DB("UserAddGUI/konstruktor");
        JPanel jpanel = new JPanel();
        jpanel.setPreferredSize(new Dimension(100,200));
//        userNameField.setMinimumSize(new Dimension(100,100));
        userNameField.setPreferredSize(new Dimension(100,20));
        jpanel.add(usernameLabel);
        jpanel.add(userNameField);
        jpanel.add(dodajButton);
        jpanel.add(cancelButton);

        add(jpanel);


        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                removeAll();
                new MainMenu();

            }
        });

        dodajButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                User user = new User(db.getUsers().size()+1,userNameField.getText());
                db.insertUser(user);

                removeAll();
                new MainMenu();
            }
        });
    }
}
