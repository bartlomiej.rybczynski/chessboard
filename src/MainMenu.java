import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainMenu extends JFrame{
    private JButton startNewGameButton;
    private JButton loadGameButton;
    private JButton showHistoryButton;
    public JPanel panelMain;
    private JTextField statusMenuTextField;
    private JButton uzytkownicyButton;
    private JTextField loadGameTextField;

    //    DB db = new DB();
//    DB db;
    public MainMenu() {
        super("kosci");
        getContentPane().add(panelMain);
        DB db = new DB("MainMenu/konstruktor");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(200,250);
        setLocation((int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2 - getSize().width),(int)Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2 -getSize().height);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        statusMenuTextField.setText("Kosci v 0.1 - wybierz opcje");
        setVisible(true);
        loadGameTextField.setSize(20,16);


        startNewGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                DB db = new DB("MainMenu/startNewGameButtonListener");
                statusMenuTextField.setText("Start Button Clicked!");
                db.startNewGame(db.getGameRows(),db.getUsers());
                getContentPane().removeAll();
                db = null;
                getContentPane().add(new GameGUI("NEW"));
                setSize(800,800);
//                setExtendedState(JFrame.MAXIMIZED_BOTH);
//                GameGUI jPanel = new GameGUI();
//                getContentPane().add(jPanel);
//                new GameGUI();
//                swapPanel();
                setVisible(true);

            }
        });
        uzytkownicyButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                getContentPane().removeAll();
                getContentPane().add(new UserAddGUI());
                setVisible(true);
            }
        });
        loadGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setVisible(false);
                getContentPane().removeAll();
                getContentPane().add(new GameGUI(loadGameTextField.getText()));
                setSize(800,800);
                setVisible(true);
//                setSize(200,300);
//                repaint();
//                JTextField gameid = new JTextField();
//                System.out.println("LOAD klikniety");
//                JOptionPane optionPane = new JOptionPane();
//                gameid.setSize(20,16);
//                add(gameid);
            }
        });
    }

    protected void swapPanel() {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//
//                removeAll();
//                add(new GameGUI());
//                invalidate();
//                revalidate();
//
//            }
//
//        });
//        JPanel jPanel = new GameGUI();
//        this.add(jPanel,BorderLayout.CENTER);
    }

//    public static void main(String[] args){
//        JFrame frame = new JFrame();
//        frame.setContentPane(new MainMenu().panelMain);
//        frame.pack();
//        frame.setVisible(true);
//    }


}
