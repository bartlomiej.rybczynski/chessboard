public class User {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public User(int id, String name){
        setId(id);
        setName(name);
    }
    private String name;
    private int id;
}
