import javax.swing.text.html.HTMLDocument;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class DB {
    private PreparedStatement ps;

    private Connection getConnection() {
        return connection;
    }

    private void setConnection(Connection connection) {
        this.connection = connection;
    }

    private Connection connection;

    private Connection connect() {
        Connection connection = null;
        try {
            String url = "jdbc:sqlite:kosci.db";
            connection = DriverManager.getConnection(url);
//            System.out.println("DB Connected!");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        setConnection(connection);
        return connection;
    }

    public DB(String classname) {
        File file = new File("kosci.db");
//        System.out.println("Polaczenie z " + classname + " ustanowione");
        if (!file.exists()) {
            System.out.println("Database does not exist - creating tables");
            Connection connection = connect();
            checkIfCreated(connection);
        } else if (connection == null) {
            Connection connection = connect();
        }
//        System.out.println("Polaczenie z " + classname + " przerwane");
    }

    @Override
    public void finalize() throws Throwable {
        connection.close();
    }

    private void checkIfCreated(Connection connection) {
        ps = null;
        try {
            connection = connect();

            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS users(" +
                    "userid INTEGER NOT NULL PRIMARY KEY, "
                    + "username VARCHAR(10)" +
                    " );");
            ps.executeUpdate();
            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS games(" +
                    "gameid INTEGER NOT NULL PRIMARY KEY," +
                    "userid VARCHAR(10)" +
                    ");");
            ps.executeUpdate();

            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS gamescore(" +
                    "gameid INTEGER NOT NULL," +
                    "userid INTEGER NOT NULL," +
//                    "gamerowid INTEGER NOT NULL," +
                    "gamerowid VARCHAR(10) NOT NULL," +
                    "value VARCHAR(10)" +
                    ");");
            ps.executeUpdate();

            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS gamerow(" +
                    "gamerowid INTEGER NOT NULL PRIMARY KEY," +
                    "gamerowname VARCHAR(10)" +
                    ");");
            ps.executeUpdate();
            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS totalscore(" +
                    "userid INTEGER NOT NULL ," +
                    "value VARCHAR(10)," +
                    "gameid VARCHAR(10)" +
                    ");");
            ps.executeUpdate();
            initGameRow();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void startNewGame(ArrayList<GameRow> gameRowArrayList, ArrayList<User> users) {
        int gameid = getNewGameId();
        for (User user : users) {
            for (GameRow gr : gameRowArrayList) {
//                System.out.println(user.getName() + " " + gr.getName());

                initGame(user, gr, gameid);
            }

            insertTotalScoreForGame(user, gameid);
        }
    }

    private int getNewGameId() {

        PreparedStatement stmt = null;

        try {
            String sql = "SELECT max(gameid)+1 as gameid from gamescore;";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            return rs.getInt("gameid");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    private void insertTotalScoreForGame(User user, int gameid) {
        PreparedStatement stmt = null;
        String sql = "INSERT INTO totalscore(gameid,userid,value) values(?,?,?);";
//        String sql = "INSERT INTO gamescore (gameid,userid,gamerowid,value) values (?,?,?,'')";
        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, gameid);
            stmt.setInt(2, user.getId());
            stmt.setInt(3, 0);

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    protected void updateTotalScore(User user, int gameid, int value) {
        String sql = "UPDATE totalscore set value =? where gameid=? and userid =?;";
        PreparedStatement stmt = null;
        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, value );//(+ getValueForTotalScore(user, gameid));
            stmt.setInt(2, gameid);
            stmt.setInt(3, getUserDetails(user).getId());
            //System.out.println("Gameid: " + gameid + " user " +getUserDetails(user).getId() );

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private int getValueForTotalScore(User user, int gameid) {
        String sql = "select sum(value) from totalscore where userid=? ;";
        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, getUserDetails(user).getId());
            stmt.setInt(2, gameid);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                return rs.getInt("value");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    protected void updateGameRow(User user, GameRow gameRow, int currentGame) {
        PreparedStatement stmt = null;
        String sql = "UPDATE gamescore set value =? where gameid=? and userid=? and gamerowid=?";
        try {
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setString(1, gameRow.getValue());
            stmt.setInt(2, currentGame);
            stmt.setInt(3, getUserDetails(user).getId());
            stmt.setInt(4, gameRow.gamerowid);
//            System.out.println(gameRow.getValue() + " " + currentGame + " " + user.getId() + " " + gameRow.gamerowid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private void initGame(User user, GameRow gameRowArrayList, int gameid) {
        PreparedStatement stmt = null;

        String sql = "INSERT INTO gamescore (gameid,userid,gamerowid,value) values (?,?,?,'')";
        try {
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, gameid);
            stmt.setInt(2, user.getId());
            stmt.setInt(3, gameRowArrayList.getGamerowid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void initGameRow() {


        String[] rundy = {"Jedynki", "Dwojki", "Trojki", "Czworki", "Piatki", "Szostki",
                "1 Para", "2 Pary", "Trójka", "Full", "M.Strit", "D.Strit", "Kareta", "Poker",
                "Parzyste", "Nieparzyste", "Szansa"};

        String sql = "INSERT INTO gamerow(gamerowname) VALUES (?);";
        PreparedStatement stmt = null;
        for (String s : rundy) {
            try {
                connection = connect();

                stmt = connection.prepareStatement(sql);
                stmt.setString(1, s);
                stmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    stmt.close();
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    protected void insertUser(User user) {
        PreparedStatement stmt = null;

        String sql = "INSERT INTO users(userid,username) values (?,?)";
        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, user.getId());
            stmt.setString(2, user.getName());


            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public ArrayList<GameRow> getGameRows() {
        PreparedStatement stmt = null;
        ArrayList gameRowArrayList = new ArrayList();

        try {
            String sql = "SELECT * from gamerow;";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                GameRow gr = new GameRow(rs.getString("gamerowid"), rs.getString("gamerowname"), rs.getInt("gamerowid"));
                gameRowArrayList.add(gr);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return gameRowArrayList;
    }

    public ArrayList<User> getUsers() {
        PreparedStatement stmt = null;
        ArrayList userArray = new ArrayList();

        try {
            String sql = "SELECT * from users;";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("userid"), rs.getString("username"));
                userArray.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return userArray;
    }

    public User getUserDetails(User user) {
        PreparedStatement stmt = null;
        ArrayList userArray = new ArrayList();

        try {
            String sql = "SELECT * from users where username=?;";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getName());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getInt("userid"), user.getName());
                return u;
//                userArray.add(u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return null;
    }


    public String getScore(User user, GameRow gameRow, int gameid) {
        PreparedStatement stmt = null;
        ArrayList userArray = new ArrayList();

        try {
            String sql = "SELECT value from gamescore where userid =? and gamerowid =? and gameid=?;"; //TODO : dodaj GAMEID
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, getUserDetails(user).getId());
            stmt.setInt(2, gameRow.getGamerowid());
            stmt.setInt(3, gameid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getString("value");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    public int getGameRow(String gamerowName) {
        PreparedStatement stmt = null;

        try {
            String sql = "SELECT gamerowid from gamerow where gamerowname =?;"; //TODO : dodaj GAMEID
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setString(1, gamerowName);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("gamerowid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public String getTotalSummaryScore(User user) {
        PreparedStatement stmt = null;
        ArrayList<String> lista = new ArrayList();
        try {
            String sql = "SELECT sum(value) as summary from totalscore where userid=? ";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, user.getId());
//            stmt.setInt(2, gameid);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getString("summary");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

//        int penality = 0;
//        int bonus = 0;
//        int score = 0;
//        if (checkSchool(lista) == false) {
//            penality = 50;
////            System.out.println("penality scored");
//        } else {
//            if (checkBonus(lista)) {
//                bonus = 100;
//            }
//        }
//
//        for (String s : lista) {
////            System.out.println(score);
//            if (!s.equals("")) {
//                score += Integer.parseInt(s);
//            }
//        }
//        score += bonus - penality;

        return "";
    }

    public String getSummaryScore(User user, int gameid) {
        PreparedStatement stmt = null;
        ArrayList<String> lista = new ArrayList();
        int penality = 0;
        int bonus = 0;
        int score = 0;
        try {
            String sql = "SELECT value as summary from gamescore where userid=? and gameid=?";
            connection = connect();

            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, user.getId());
            stmt.setInt(2, gameid);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
//                System.out.println(rs.getString("summary"));
                lista.add(rs.getString("summary"));
//                return rs.getString("summary");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (checkSchool(lista) == false) {
            penality = 50;
//            System.out.println("penality scored");
        } else {
            if (checkBonus(lista)) {
                bonus = 100;
            }
        }

        for (String s : lista) {
//            System.out.println(score);
            if (!s.equals("")) {
                score += Integer.parseInt(s);
            }
        }
        score += bonus - penality;
        updateTotalScore(user, gameid, score);

        return String.valueOf(score);
    }

    protected Boolean checkSchool(ArrayList<String> lista) {
        int i = 0;
        int listaSchool = 0;
        int s = 0;
        for (String l : lista) {
            if (!l.equals("")) {

                s = Integer.parseInt(l);
                if (i < 6) {
//                    System.out.print("S to : " + s);

                    listaSchool += s;//Integer.parseInt(s.replace("", "0"));
                }
            }
            i++;
        }
        if (listaSchool < 0) {
//            System.out.println("checkBonus : false");
            return false;

        }
        return true;
    }

    protected Boolean checkBonus(ArrayList<String> lista) {
        int i = 0;
        int s = 0;
        for (String l : lista) {
            if (!l.equals("")) {
                s = Integer.parseInt(l);
                if (i > 5) {
//                    if (Integer.parseInt(s.replace("", "0")) <= 0) {
                    if (s <= 0) {
                        return false;
                    }
                }
                i++;
            } else {
                return false;
            }
        }
        return true;
    }

    public String getMaxGameId() {
        String sql = "select max(gameid) as gameid from gamescore;";
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getString("gameid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return "";
    }
}
