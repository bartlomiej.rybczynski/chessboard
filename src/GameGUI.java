import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class GameGUI extends JPanel {
    //    DB db ;
    private JPanel gameGUIpanel;
    JLabel summary;

    public GameGUI(String gameID) {
        if (gameID.equals("NEW")) {
            DB db = new DB("GameGUI/Konstruktor/ustawianie gameid");
            gameID = db.getMaxGameId();
        }
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JTextArea logArea = new JTextArea();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        logArea.append("Start gry nr: " + gameID + " data: " + dateFormat.format(date) + "\n");
        Font font = new Font("Arial", Font.PLAIN, 32);
        DB db = new DB("GameGUI/konstruktor");


        DefaultTableModel defaultTableModel = new DefaultTableModel();
        defaultTableModel.addColumn("Nazwa");
        DefaultTableModel summaryModel = new DefaultTableModel();
        summaryModel.addColumn("");



        ArrayList<User> users = db.getUsers();

        for (User user : users) {
            defaultTableModel.addColumn(user.getName());
            summaryModel.addColumn("");
        }
//        JPanel totalSummary = TotalScore(users);
//        add(totalSummary);
        for (GameRow gr : db.getGameRows()) {
            String[] data = new String[users.size() + 1];
            data[0] = gr.getName();
            int i = 1;

            for (User u : users) {
//                System.out.println(db.getScore(u,gr));
                data[i] = db.getScore(u, gr, Integer.parseInt(gameID));
                i++;
            }

            defaultTableModel.addRow(data);
        }


        JTable jtable = new JTable(defaultTableModel);
        jtable.getTableHeader().setFont(new Font("Sans Serif", Font.BOLD, 24));

//        JTableHeader th = jtable.getTableHeader();
//        th.setPreferredSize(new Dimension(100, 100));
        jtable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
            int i = 0;

            @Override
            public Component getTableCellRendererComponent(JTable table,
                                                           Object value, boolean isSelected, boolean hasFocus, int row, int col) {

                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
//                setFont(new Font("Arial",Font.BOLD,24));
//                String status = (String)table.getModel().getValueAt(row, 0);
                if (row < 6) {


                    setBackground(Color.ORANGE);
                    setForeground(table.getForeground());
//                }
                } else if (row % 2 == 0) {

                        setBackground(Color.darkGray);
                        setForeground(Color.WHITE);


                } else {
//                    setBackground(table.getBackground());
//                    setForeground(table.getForeground());
                    setBackground(Color.WHITE);
                    setForeground(Color.BLACK);
                }

                if (isSelected) {
//                    System.out.println(row);
                    setBackground(Color.RED);
                    setForeground(Color.WHITE);
                }
//                getTableCellRendererComponent2(table,value,isSelected,hasFocus,row,col,jtable.getSelectedColumn());

//                    Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
//                    if (col == jtable.getSelectedColumn() ) {
//                        ColorHeaderValue
//                        getTableCellRendererComponent2(table,value,isSelected,hasFocus,row,col,jtable.getSelectedColumn());
//                        defaultTableModel.set`
//                        setBackground(Color.BLACK);
//                    }

                //                    getTableCellRendererComponent2()
//                    getTableCellRendererComponent(table)
//                    System.out.println(jtable.getColumnName(jtable.getSelectedColumn()));
//                    System.out.println("BUM");
//                }
                return this;

            }

            public Component getTableCellRendererComponent2(JTable table,
                                                            Object value, boolean isSelected, boolean hasFocus, int row, int col, int selCol) {
                super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
//                System.out.println("row:" + row + "col: " + col + " selcol: " + selCol);
//                System.out.println(i++);
//                repaint();
//                if (col == selCol){
                if (col == selCol) {
                    DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
                    headerRenderer.setBackground(new Color(239, 198, 46));
//                        defaultTableModel
//                    System.out.println("BUM");

                    setBackground(Color.BLUE);
                    return this;
//                }else{
//                    setBackground(getBackground());
                }
//                    System.out.println(col);
                return this;
            }


        });
        jtable.setFont(font);
        jtable.setRowHeight(30);
//db=null;

        String finalGameID = gameID;
//        jtable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
//            public void valueChanged(ListSelectionEvent event) {
//                // do some actions here, for example
//                // print first column value from selected row
////                System.out.println(jtable.getValueAt(jtable.getSelectedRow(), 0).toString());
//                    event. jtable.getSelectedRow()
////                setBackground(Color.lightGray);
////                setForeground(Color.WHITE);
//
//
//            }
//        });
        jtable.getModel().addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                DB db2 = new DB("GAMEGUI/jtabellistener");
//                System.out.println(e.getColumn() + " " + e.getFirstRow());
//                System.out.println(defaultTableModel.getRowCount());
//                if (defaultTableModel.getRowCount() == 17) {
//                    System.out.println("IF");
                String newvalue = String.valueOf(jtable.getValueAt(e.getFirstRow(), e.getColumn()));
                String gamerowName = (String) jtable.getValueAt(e.getFirstRow(), 0);

                User user = new User(1, jtable.getColumnName(e.getColumn()));
                GameRow gr = new GameRow(newvalue, gamerowName, db2.getGameRow(gamerowName));
//                System.out.println(gr.getGamerowid() + " " + gr.getName() + " " + gr.getValue());
//User user, GameRow gameRow, int currentGame)
                db2.updateGameRow(user, gr, Integer.parseInt(finalGameID));
//                    System.out.println(newvalue); //wynik
                summary.setText(TotalScore(users));
                summaryModel.removeRow(0);
//                    System.out.println("Pod Ifem");
                String[] data = new String[users.size() + 1];
                data[0] = "Suma ";
                int i = 1;
                for (User u : users) {
//                System.out.println(db.getScore(u,gr));
                    data[i] = db.getSummaryScore(u, Integer.parseInt(finalGameID));
                    i++;
                }

                summaryModel.addRow(data);
                logArea.append(user.getName() + " rzuca wynik: " + newvalue + " dla rundy: " + gr.getName() + "\n");
//                remove(totalSummary);
//                }
//
//                add(TotalScore(users));
                repaint();

            }
        });


        String[] data = new String[users.size() + 1];
        data[0] = "Suma";
        int i = 1;
        for (User u : users) {
//                System.out.println(db.getScore(u,gr));
            data[i] = db.getSummaryScore(u, Integer.parseInt(gameID));
            i++;
        }
        summaryModel.addRow(data);
        JTable summaryTable = new JTable(summaryModel);
        JScrollPane summaryPanel = new JScrollPane(summaryTable);


        JScrollPane jsp = new JScrollPane(jtable);
        summary = new JLabel();
        summary.setText(TotalScore(users));
        add(summary);
//        System.out.println(getFont());
        setFont(font);
        jsp.setPreferredSize(new Dimension((users.size() + 1) * 150, getFont().getSize() * 17+10));
//        jsp.setPreferredSize(new Dimension((users.size() + 1) * 150, 200));
        summaryPanel.setPreferredSize(new Dimension((users.size() + 1) * 150, 40));//getFont().getSize() * 16));
        summaryTable.setFont(font);
        summaryTable.setRowHeight(30);
//        summaryPanel.getColumnHeader().setFont(new Font ("Sans", Font.PLAIN, 30));
        jsp.setBorder(BorderFactory.createEmptyBorder());
        summaryPanel.setBorder(BorderFactory.createEmptyBorder());
//        summaryPanel.setFont(new Font("Sans Serif", Font.PLAIN, 20));
//        System.out.println(getFont());
        setPreferredSize(new Dimension(users.size() * 150, getFont().getSize() * 32));
        JPanel content = new JPanel();
        content.add(jsp);
        content.add(summaryPanel);
        logArea.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(logArea);
        logScrollPane.setPreferredSize(new Dimension((users.size() + 1) * 150, 100));
        logScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
//        logScrollPane.add(logArea);
        content.add(logScrollPane);

//
//        add(jsp);
//        add(summaryPanel);
//        remove(podsumowanie);
        add(content);
        setVisible(true);

    }

    public String TotalScore(ArrayList<User> users) {
        DB db = new DB("GameGUI/TotalScore");
        String tabSummary = "Suma ( TOTAL ) >> ";
        for (User user : users) {
            tabSummary += " " + user.getName() + " : " + db.getTotalSummaryScore(user) + "  ";

        }
        return tabSummary;
    }

//todo : powiekszyc wszytko bo gramy ze staremy ludzmi
}
